<h1>Logger</h1>
<h3>How to install</h3>
<p>To use the logger you need to copy Logger-1.0-SNAPSHOT.jar 
file. The you need to add this jar file dependency to your 
project. If you use IntelliJ IDEA, you can find out how to 
use it <a href="https://stackoverflow.com/questions/105164
0/correct-way-to-add-external-jars-lib-jar-to-an-intellij-ide
a-project">here</a>. If you cannot do it you can just copy 
logger package (Logger/src/main/java/logger). This package 
contains everything you need for convenient logging.</p>
<h3>How to use</h3>
<p>To use the logger you need to initialize Logger object.
If you want you can set Handler object for this logger 
(StandardHandler, FileHandler, BlackHoleHandler).
You can also set Formatter object. Creating Formatter 
object you should specify what parts of logging message you
want to see. Most advanced users can set ArrayList of OutputStream
objects as a value of streamDependency  
HashMap (attribute of Handler object) where LogLevel is the 
key in this map. 
</p>
<h4>Create default logger</h4>
<code>Logger logger = new Logger("Tests");</code>
<h4>Create logger with predefined LogLevel</h4>
<code>Logger logger = new Logger (LogLevel.warning, "Tests");</code>
<h4>Set LogLevel afterwards</h4>
<code>logger.setLogLevel(LogLevel.trace);</code>
<h4>Set handler for this logger</h4>
<code>logger.setHandler(new StandardHandler())</code>
<h4>Set fileHandler for the logger</h4>
<code>logger.setHandler(new FileHandler("path/to/directory"));</code>
<h4>Log a message</h4>
<code>logger.log(LogLevel.warning, "message")</code>
<h4>Simplified way to log messages.</h4>
<code>logger.fatal("message")</code>
<h4>Log a message specifying OutPutStream for particular logMessage.</h4>
<code>logger.log(LogLevel.warning, "message", System.out)</code>
<h4>create a log using lazy calculation</h4>
<code>Logger.StringFunction func = () -> {return "1" + " " + "message";};<br>
logger.lazyLog(LogLevel.error, func);</code>
<h4>Set fileHandler for the logger</h4>
<code>logger.setHandler(new FileHandler("path/to/directory"));</code>
<h4>Set BlackHoleHandler for the logger to send logMessages directly
to nowhere....</h4>
<code>logger.setHandler(new BlackHoleHandler());</code>
<h4>Set output streams for specific LogLevels</h4>
<code>ArrayList<OutputStream> streams = new ArrayList<>();<br>
streams.add(System.out);<br>streams.add(System.out);<br>
handler.streamDependency.replace(LogLevel.error, nullArray);</code>
<h4>Set Formatter that formats log message string to show specific parts
of LogMessage object (program component, date, log level)</h4>
<code>logger.setFormatter(new Formatter(true, true, false));</code>
<h4>You can also change the function determining the time of log message.</h4>
<code>Logger.DateFunction getDate = () -> {return new Date();};<br>
logger.getDate = getDate.run();</code>
<h3>All other scenarios of the Logger use you can find in test directory of the project.</h3>
<h3>P.S. You can get familiar with all the use cases in the reading the tests.</h3>
