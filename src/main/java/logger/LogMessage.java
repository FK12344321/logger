package logger;

import java.util.Date;

public class LogMessage {
    private final StackTraceElement component;
    private final Date date;
    private final LogLevel level;
    private final String message;

    public LogMessage(StackTraceElement component, Date date, LogLevel level, String message) {
        this.component = component;
        this.date = date;
        this.level = level;
        this.message = message;
    }

    public StackTraceElement getComponent() {
        return this.component;
    }

    public Date getDate() {
        return this.date;
    }

    public LogLevel getLevel() {
        return this.level;
    }

    public String getMessage() {
        return message;
    }
}
