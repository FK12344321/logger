package logger;

import java.io.OutputStream;
import java.util.ArrayList;

public class StandardHandler extends Handler {
    public StandardHandler() {
        for (LogLevel level : LogLevel.values()) {
            OutputStream out = level.compareTo(LogLevel.info) > 0 ? System.out : System.err;
            ArrayList<OutputStream> streams = new ArrayList<>();
            streams.add(out);
            streamDependency.put(level, streams);
        }
    }
}
