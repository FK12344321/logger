package logger;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

interface Log {
    void log(LogLevel level, String message) throws IOException;
}

public class Logger implements Log {
    private Formatter formatter;
    private Handler handler;
    private LogLevel level;
    private final String baseClassName;
    public interface DateFunction { Date run(); }
    public DateFunction getDate = () -> {return new Date();};

    public Logger(LogLevel level, String baseClassName) {
        this.level = level;
        this.baseClassName = baseClassName;
        this.handler = new StandardHandler();
        this.formatter = new Formatter();
    }

    public Logger(String baseClassName) {
        this(LogLevel.info, baseClassName);
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public void setFormatter(Formatter formatter) {
        this.formatter = formatter;
    }

    private StackTraceElement findComponent() {
        Throwable t = new Throwable();
        StackTraceElement[] trace = t.getStackTrace();
        for (StackTraceElement stackTraceElement : trace) {
            if (stackTraceElement.getClassName().equals(baseClassName)) {
                return stackTraceElement;
            }
        }
        return null;
    }

    private LogMessage createLogMessage(LogLevel level, String message) {
        return new LogMessage(findComponent(), getDate.run(), level, message);
    }

    public interface StringFunction { String run(); }

    public void lazyLog(LogLevel messageLevel, StringFunction str) throws IOException {
        if (this.level.compareTo(messageLevel) > 0) return;
        LogMessage logMessage = createLogMessage(messageLevel, str.run());
        handler.output(formatter.format(logMessage), messageLevel);
    }

    public void log(LogLevel messageLevel, String message) throws IOException {
        if (this.level.compareTo(messageLevel) > 0) return;
        LogMessage logMessage = createLogMessage(messageLevel, message);
        handler.output(formatter.format(logMessage), messageLevel);
    }

    public void log(LogLevel messageLevel, String message, OutputStream out) throws IOException {
        if (this.level.compareTo(messageLevel) > 0) return;
        LogMessage logMessage = createLogMessage(messageLevel, message);
        handler.output(formatter.format(logMessage), out);
    }

    public void trace(String message) throws IOException {
        log(LogLevel.trace, message);
    }

    public void debug(String message) throws IOException {
        log(LogLevel.debug, message);
    }

    public void info(String message) throws IOException {
        log(LogLevel.info, message);
    }

    public void warning(String message) throws IOException {
        log(LogLevel.warning, message);
    }

    public void error(String message) throws IOException {
        log(LogLevel.error, message);
    }

    public void fatal(String message) throws IOException {
        log(LogLevel.fatal, message);
    }

    public void setLogLevel(LogLevel level) {
        this.level = level;
    }
}
