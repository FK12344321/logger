package logger;

public enum LogLevel {
    trace,
    debug,
    info,
    warning,
    error,
    fatal,
}
