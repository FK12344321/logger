package logger;

import java.util.*;
import java.io.*;

public class BlackHoleHandler extends Handler {
    public BlackHoleHandler() {
        for (LogLevel level : LogLevel.values()) {
            ArrayList<OutputStream> streams = new ArrayList<>();
            streams.add(null);
            streamDependency.put(level, streams);
        }
    }
}
