package logger;

import java.io.IOException;
import java.util.*;
import java.io.*;

public class FileHandler extends Handler{
    public FileHandler(String path) throws IOException {
        for (LogLevel level : LogLevel.values()) {
            String tempPath = path + "/" + level.name() + ".txt";
            File file = new File(tempPath);
            OutputStream out = new FileOutputStream(tempPath);
            ArrayList<OutputStream> streams = new ArrayList<>();
            streams.add(out);
            streamDependency.put(level, streams);
        }
    }
}
