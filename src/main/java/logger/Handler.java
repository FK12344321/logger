package logger;

import java.io.*;
import java.util.*;

abstract public class Handler {
    public HashMap<LogLevel, ArrayList<? extends OutputStream>> streamDependency = new HashMap<>();

    public void output(String logMessage, LogLevel level) throws IOException {
        for (OutputStream out : streamDependency.get(level)) {
            //if (out == null) continue;
            out.write((logMessage + "\n").getBytes());
        }
    }

    public void output(String logMessage, OutputStream out) throws IOException {
        if (out == null) return;
        out.write((logMessage + "\n").getBytes());
    }
}
