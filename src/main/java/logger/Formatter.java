package logger;

import java.util.Date;
import java.text.SimpleDateFormat;

public class Formatter {
    private final boolean showComponent;
    private final boolean showDate;
    private final boolean showLevel;

    public Formatter(boolean showComponent, boolean showDate, boolean showLevel) {
        this.showComponent = showComponent;
        this.showDate = showDate;
        this.showLevel = showLevel;
    }

    public Formatter() {
        this.showComponent = false;
        this.showDate = true;
        this.showLevel = true;
    }

    public String formatDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return "[" + formatter.format(date) + "]";
    }

    public String formatComponent(StackTraceElement component) {
        if (component == null) return "[-]";
        return "[" + component.getClassName() + " " + component.getMethodName() + "]";
    }

    public String formatLogLevel(LogLevel level) {
        return "[" + level.name() + "]";
    }

    public String format(LogMessage logMessage) {
        String result = "";
        if (showComponent) result += formatComponent(logMessage.getComponent()) + " ";
        if (showDate) result += formatDate(logMessage.getDate()) + " ";
        if (showLevel) result += formatLogLevel(logMessage.getLevel()) + " ";
        result += logMessage.getMessage();
        return result;
    }
}
