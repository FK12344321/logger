import org.junit.jupiter.api.Test;
import logger.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.*;
import java.util.ArrayList;
import org.junit.jupiter.api.Assertions;

public class Tests {
    private final String journalPath = System.getProperty("user.dir") + "/journal";
    private final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    Logger.DateFunction getDate = () -> {
        Date date = new Date();
        date.setTime(0);
        return date;
    };

    private final String date = "[" + formatter.format(getDate.run()) + "]";

    private boolean compareWithFileContent(String path, String str, int lines) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(path));
        StringBuilder fileContent = new StringBuilder();
        for (int i = 0; i < lines; i++) {
            fileContent.append(br.readLine());
        }
        if (str == null) return fileContent.toString().equals("null");
        return fileContent.toString().equals(str);
    }

    @Test
    void test1() throws IOException {
        Logger logger = new Logger("Tests");
        logger.getDate = this.getDate;
        logger.setHandler(new FileHandler(journalPath));
        logger.info("1");
        logger.info("2");
        Assertions.assertTrue(
                compareWithFileContent(this.journalPath + "/info.txt",
                        this.date + " [info] 1" + this.date + " [info] 2", 2)
        );
    }

    @Test
    void test2() throws IOException {
        Logger logger = new Logger(LogLevel.error, "Tests");
        logger.getDate = this.getDate;
        logger.setHandler(new FileHandler(journalPath));
        logger.setFormatter(new Formatter(true, true, true));
        logger.log(LogLevel.fatal, "fatal level");
        logger.setFormatter(new Formatter(true, false, true));
        logger.log(LogLevel.error, "error level");
        logger.log(LogLevel.debug, "debug level");
        Assertions.assertTrue(
                compareWithFileContent(this.journalPath + "/fatal.txt",
                        "[Tests test2] " + this.date + " [fatal] fatal level", 1)
        );
        Assertions.assertTrue(
                compareWithFileContent(this.journalPath + "/error.txt",
                        "[Tests test2] [error] error level", 1)
        );
        Assertions.assertTrue(
                compareWithFileContent(this.journalPath + "/debug.txt",
                        null, 1)
        );
    }

    @Test
    void test3() throws IOException {
        Logger logger = new Logger(LogLevel.error, "Tests");
        logger.getDate = this.getDate;
        logger.setHandler(new FileHandler(journalPath));
        logger.setHandler(new StandardHandler());
        logger.info("info log");
        logger.setHandler(new FileHandler(journalPath));
        logger.setLogLevel(LogLevel.trace);
        logger.trace("1");
        logger.debug("2");
        logger.info("3");
        logger.error("4");
        logger.fatal("5");
        logger.warning("6");
        Assertions.assertTrue(
                compareWithFileContent(this.journalPath + "/trace.txt",
                        this.date + " [trace] 1", 1)
        );
        Assertions.assertTrue(
                compareWithFileContent(this.journalPath + "/debug.txt",
                        this.date + " [debug] 2", 1)
        );
        Assertions.assertTrue(
                compareWithFileContent(this.journalPath + "/info.txt",
                        this.date + " [info] 3", 1)
        );
        Assertions.assertTrue(
                compareWithFileContent(this.journalPath + "/error.txt",
                        this.date + " [error] 4", 1)
        );
        Assertions.assertTrue(
                compareWithFileContent(this.journalPath + "/fatal.txt",
                        this.date + " [fatal] 5", 1)
        );
        Assertions.assertTrue(
                compareWithFileContent(this.journalPath + "/warning.txt",
                        this.date + " [warning] 6", 1)
        );
    }

    @Test
    void test4() throws IOException {
        Logger logger = new Logger(LogLevel.error, "Tests");
        logger.warning("not testable statement");
        logger.setHandler(new BlackHoleHandler());
        logger.getDate = this.getDate;
        logger.log(LogLevel.error, "4", new FileOutputStream(journalPath + "/error.txt"));
        Assertions.assertTrue(
                compareWithFileContent(this.journalPath + "/error.txt",
                        this.date + " [error] 4", 1)
        );
    }

    @Test
    void test5() throws IOException {
        Logger logger = new Logger("SomeOtherClass");
        logger.getDate = this.getDate;
        Logger.StringFunction str = () -> {return "specific message";};
        logger.setHandler(new FileHandler(journalPath));
        logger.lazyLog(LogLevel.warning, str);
        logger.lazyLog(LogLevel.trace, str);
        Assertions.assertTrue(
                compareWithFileContent(this.journalPath + "/warning.txt",
                        this.date + " [warning] specific message", 1)
        );
        Assertions.assertTrue(
                compareWithFileContent(this.journalPath + "/trace.txt",
                        null, 1)
        );
    }

    @Test
    void test6() throws IOException {
        Logger logger = new Logger("SomeOtherClass");
        StandardHandler handler = new StandardHandler();
        ArrayList<OutputStream> nullArray = new ArrayList<>();
        nullArray.add(null);
        handler.streamDependency.replace(LogLevel.error, nullArray);
        logger.warning("not testable");
        logger.log(LogLevel.fatal, "to the hell");
        logger.log(LogLevel.error, "to the hell as well", null);
        logger.getDate = this.getDate;
        logger.setHandler(new FileHandler(journalPath));
        logger.setFormatter(new Formatter(true, false, false));
        logger.log(LogLevel.error, "undefined base class");
        logger.log(LogLevel.trace, "shall not pass!!!", System.out);
        Assertions.assertTrue(
                compareWithFileContent(this.journalPath + "/error.txt",
                        "[-] undefined base class", 1)
        );
    }
}
